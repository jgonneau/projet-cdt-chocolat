## Contributing

Merci de nous faire une 'Pull Request' si vous souhaitez contribuer et d'y définir précisément le but ce celle-ci.

N'hésitez pas à venir discuter de toutes sortes potentielles améliorations ou ajouts.
