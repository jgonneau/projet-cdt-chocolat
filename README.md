[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](code-of-conduct.md)

# Solution cahier de textes "Chocolat"

Cahier de textes appelée quelque fois « Chocolat » est une application gratuite utilisée aujourd’hui par des milliers d’enseignants. Sa pertinence lui vaut d’être intégrée aujourd’hui dans de nombreux ENT académiques ( Académies de Créteil, Marseille, Orléans-Tours, Dijon, Poitiers…) ou simplement distribuée de façon autonome (Académie d’Amiens, Lille, Rouen, Corse, Guadeloupe, Martinique, Montpellier…). Enfin, certains l’utilisent en parallèle de solutions ENT commerciales (Pronote, OMT, Aplon…) jugeant leur module incomplet ou peu ergonomique. L’application respecte le cahier des charges fixées par le ministère, son auteur ayant d’ailleurs contribué à l’élaboration de celui-ci.

## Cahier de textes en quelques mots…

Cahier de textes se différencie des autres applications par :

<ul>
    <li>Sa facilité de prise en main et ses puissantes fonctionnalités.</li>
    <li>L’intégration de l’emploi du temps de l’enseignant facilitant considérablement la saisie.</li>
    <li>L’implantation facultative d’un module de déclaration d’absences.</li>
</ul>

Cette application développée par Pierre Lemaitre, enseignant de l’académie de Caen, est le fruit de nombreuses concertations entre enseignants, élèves et acteurs des établissements. Ces échanges permanents ont orienté son développement. Elle est ainsi devenue l’outil le plus abouti de son domaine et le plus plébiscité par les enseignants. Ses différents paramétrages en font une application très ouverte, à la fois facile à prendre en main par les débutants et répondant aux besoins des plus exigeants. Ses mises à jour régulières témoignent de cette dynamique. L’application peut être implantée sur tout hébergement Web offrant la gestion du couple Php/Mysql. (Hébergements académiques, OVH, Free… ).

## Licence

Cette application relève du domaine du logiciel libre, sous licence GPLv3.

<hr>

## Une question ?

Vous pouvez nous contacter via cette adresse e-mail : <a href="mailto:jgonneau@j-p.work">jgonneau@j-p.work</a>
