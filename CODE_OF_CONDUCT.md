## Modifications

Chaque modification du code source, comme défini dans la licence GPL v3, doit être explicitement décrit dans le header de chaque fichier de son code source.

De par ce fait, la description doit être écrite comme suit :

" - YYYY-MM-DD, <courte-description> (l° <numero-ligne-changée>), par <nom-login>
| <-----    longue description complète    ------->
| <-----    longue description complète    ------->
